FROM registry.gitlab.com/grammatech/mnemosyne/learn-synth/snippet-mining-data

RUN mkdir /snippet-mining
COPY . /snippet-mining
WORKDIR /snippet-mining
RUN python3 setup.py bdist_wheel --universal
RUN pip3 install dist/snippet_mining-0.0.0-py2.py3-none-any.whl
WORKDIR /

CMD snippet-mining-server
