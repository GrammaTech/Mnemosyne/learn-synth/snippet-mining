"""
Database and query interface for API usage examples computed
using the the algorithm described in "Exempla Gratis (E.G.):
Code Examples for Free".
"""

from abc import ABC, abstractmethod
from copy import deepcopy
from dataclasses import dataclass
from pathlib import Path
from typing import Dict, Generator, List, Tuple, Union
from compress_pickle import loads
from kitchensink.utility.simplified_asts import SimplifiedAST
from kitchensink.utility.zodb import DatabasePool


@dataclass(eq=True, frozen=True)
class ExemplaGratisConfig:
    """
    configuration POD with fields controlling the E.G. query
    """

    k: int = 3  # number of examples to return


class ExemplaGratisDB(ABC):
    """
    represents a collection of API usage examples computed using
    the algorithm described in "Exempla Gratis (E.G.): Code Examples
    for Free"
    """

    def query(
        self,
        language: str,
        module: str,
        function: str,
        config: ExemplaGratisConfig = ExemplaGratisConfig(),
    ) -> List[SimplifiedAST]:
        """
        query the database for api usage examples computed using
        the E.G. algorithm

        :param language: source code language of the API function
        :param module: module containing the API function
        :param function: name of the API function
        :param config: configuration constants for the query
        """
        trees = self.data.get(language, {}).get(module, {}).get(function, [])
        return ExemplaGratisDB._load_trees(trees, config.k)

    def items(
        self,
    ) -> Generator[Tuple[Tuple[str, str, str], List[SimplifiedAST]], None, None]:
        """
        iterate over each API in the corpus of usage examples computed
        using E.G., yielding an API tuple (language, module, function)
        and corresponding list of example usages in turn
        """
        for language in self.data.keys():
            for module in self.data[language].keys():
                for function in self.data[language][module].keys():
                    trees = self.data[language][module][function]
                    trees = ExemplaGratisDB._load_trees(trees)
                    yield (language, module, function), trees

    @property
    @abstractmethod
    def data(self) -> Dict:
        """
        return the data to be queried
        """
        raise NotImplementedError

    @staticmethod
    def _load_trees(
        trees: Union[List[SimplifiedAST], List[bytes]],
        k: int = 3,
    ) -> List[SimplifiedAST]:
        """
        helper function to return @k uncompressed SimplifiedASTs from @trees

        :param trees: list of simplified AST trees of compressed bytes
            representing simplified AST trees
        :param k: number of trees to return
        """
        return [
            loads(tree, compression="gzip") if isinstance(tree, bytes) else tree
            for tree in trees[:k]
        ]


class InMemoryExemplaGratisDB(ExemplaGratisDB):
    """
    database where examples are stored in memory
    """

    def __init__(self, data: Dict = {}):
        self._data = data

    @property
    def data(self) -> Dict:
        return self._data


class ZODBExemplaGratisDB(InMemoryExemplaGratisDB):
    """
    database where examples are stored in a ZODB instance
    """

    DEFAULT_PATH = Path(__file__).parent / "resources" / "exempla_gratis.db"

    def __init__(self, db_path: Path = DEFAULT_PATH):
        if not db_path.exists():
            raise FileNotFoundError(f"ZODB at {db_path} does not exist.")

        database = DatabasePool.pool_create_database(db_path)
        with database.transaction() as conn:
            super().__init__(deepcopy(conn.root().data))
