"""
LSP or REST server for snippet example queries.
"""

import logging

from abc import abstractmethod
from json import dumps, loads
from pathlib import Path
from re import search, sub
from typing import Dict, List, Optional, Text, Tuple, Union

from asts import AST, ASTLanguage, ExpressionStatementAST, FieldAST, IdentifierAST
from dpcontracts import ensure, require
from flask import Flask, Response, request
from flask_restful import Api, Resource
from jsonschema import Draft7Validator
from markdown_strings import code_block
from pygls.server import LanguageServer
from pygls.lsp.methods import COMPLETION
from pygls.lsp.types import (
    CompletionItemKind,
    CompletionItem,
    CompletionList,
    CompletionParams,
    CompletionTriggerKind,
    InsertTextFormat,
    MarkupContent,
    MarkupKind,
)

from kitchensink.utility.filesystem import guess_language, slurp
from kitchensink.utility.pygls import NonTerminatingLanguageServerProtocol
from kitchensink.utility.server import lsp_arg_parser_with_rest
from kitchensink.utility.simplified_asts import SimplifiedAST

from snippet_mining.lib.database_examples import (
    DatabaseExamplesConfig,
    ZODBSimplifiedASTsDB,
)
from snippet_mining.lib.exempla_gratis import (
    ExemplaGratisConfig,
    ExemplaGratisDB,
    ZODBExemplaGratisDB,
)

ConfigType = Union[DatabaseExamplesConfig, ExemplaGratisConfig]
API_Type = Tuple[str, str, str]

DEFAULT_PORT = 3039
DEFAULT_STDIO_FLAG = False
DEFAULT_REST_FLAG = False
SCHEMAS_DIR = Path(__file__).parent / "schemas"
SUPPORTED_LANGUAGES = ["python"]


class Examples(Resource):
    """
    rest resource for retrieving examples of a function
    """

    POST_INPUT_SCHEMA = Draft7Validator(
        loads(slurp(SCHEMAS_DIR / "examples-input.json"))
    )

    POST_OUTPUT_SCHEMA = Draft7Validator(
        loads(slurp(SCHEMAS_DIR / "examples-output.json"))
    )

    @abstractmethod
    def config(self, *args: List, **kwargs: Dict) -> ConfigType:
        """
        return a config controlling the example query algorithm
        """
        raise NotImplementedError

    @abstractmethod
    def query(self, *args: List, **kwargs: Dict) -> List[SimplifiedAST]:
        """
        query for usage examples using the methodology prescribed by the subclass
        """
        raise NotImplementedError

    def post(self) -> Response:
        @require(
            "input must satisfy the JSON schema",
            lambda args: Examples.POST_INPUT_SCHEMA.is_valid(args.body),
        )
        @ensure(
            "output must satisfy the JSON schema",
            lambda args, results: Examples.POST_OUTPUT_SCHEMA.is_valid(results),
        )
        def helper(body: Dict) -> List[Text]:
            language = body.pop("language")
            module = body.pop("module")
            function = body.pop("function")
            config = self.config(**body)

            trees = self.query(language, module, function, config)
            return [tree.text() for tree in trees]

        return Response(
            dumps({"results": helper(request.get_json(force=True))}),
            status=200,
            mimetype="application/json",
        )


class DatabaseExamples(Examples):
    """
    rest resource for retrieving examples of a function directly from
    the database of simplified ASTs
    """

    def config(self, *args: List, **kwargs: Dict) -> DatabaseExamplesConfig:
        return DatabaseExamplesConfig(**kwargs)

    def query(self, *args: List, **kwargs: Dict) -> List[SimplifiedAST]:
        database = ZODBSimplifiedASTsDB()
        return database.query(*args, **kwargs)


class ExemplaGratisExamples(Examples):
    """
    rest resource for retrieving examples of a function using
    Exempla Gratis
    """

    def config(self, *args: List, **kwargs: Dict) -> ExemplaGratisConfig:
        return ExemplaGratisConfig(**kwargs)

    def query(self, *args: List, **kwargs: Dict) -> List[SimplifiedAST]:
        database = ZODBExemplaGratisDB()
        return database.query(*args, **kwargs)


def main():
    parser = lsp_arg_parser_with_rest(
        desc=__doc__,
        default_port=DEFAULT_PORT,
        default_stdio_flag=DEFAULT_STDIO_FLAG,
        default_rest_flag=DEFAULT_REST_FLAG,
    )

    args = parser.parse_args()
    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    if args.rest:
        app = Flask(__name__)
        api = Api(app)
        api.add_resource(DatabaseExamples, "/database-examples")
        api.add_resource(ExemplaGratisExamples, "/exempla-gratis-examples")

        app.run("0.0.0.0", port=args.port)
    else:

        if args.stdio:
            server = LanguageServer()
        else:
            server = LanguageServer(protocol_cls=NonTerminatingLanguageServerProtocol)

        database = ZODBExemplaGratisDB()

        # Provide snippet examples when the client invokes a completion request.
        @server.feature(COMPLETION)
        def completions(params: CompletionParams) -> CompletionList:
            def has_snippet_support(server: LanguageServer) -> bool:
                """
                Return TRUE if the language server supports snippets.
                """
                capabilities = server.client_capabilities
                capabilities = capabilities.text_document if capabilities else None
                capabilities = capabilities.completion if capabilities else None
                capabilities = capabilities.completion_item if capabilities else None
                return capabilities.snippet_support if capabilities else False

            def name_at_point(root: AST, line: int, col: int) -> Optional[AST]:
                """
                Return the name (identifier or field) at @position in @root,
                if possible.

                :param root: root node of an AST tree
                :param line: line with the AST (one-indexed)
                :param col: column with the AST (one-indexed)
                """
                node = root.ast_at_point(line, col)

                while node:
                    p = node.parent(root)
                    if not isinstance(p, (FieldAST, IdentifierAST)):
                        break
                    node = p

                if isinstance(node, (FieldAST, IdentifierAST)):
                    return node

            def skip(root: AST, node: Optional[AST]) -> bool:
                """
                Return true if we should skip generating predictions for this
                node under the cursor.

                :param root: root of the AST containing @node
                :param node: identifier or field node
                """
                while node:
                    parent = node.parent(root) if node else None
                    if isinstance(parent, ExpressionStatementAST):
                        return False
                    node = parent

                return True

            def examples(
                root: AST,
                name: AST,
                database: ExemplaGratisDB,
            ) -> Dict[API_Type, List[SimplifiedAST]]:
                """
                Return a list of possible example snippet completions for the given
                name in a script.

                :param root: root of the AST containing @name
                :param name: identifier or field node
                :param database: database of API usage examples
                """

                def build_name_api(
                    root: AST,
                    name: AST,
                ) -> API_Type:
                    """
                    Build an API triple (language, module, function) from the
                    given node composing an identifier or field name.

                    :param root: root of the AST containing @name
                    :param name: identifier or field node
                    """
                    language = name.language.name.lower()
                    module = name.provided_by(root)
                    if isinstance(name, FieldAST):
                        function = name.children[-1].source_text
                    else:
                        function = name.source_text

                    return (language, module, function)

                def contextualize_api(api: API_Type, imports: List) -> API_Type:
                    """
                    Return @api updated for the context we are inserting into.
                    Replace unaliased names with aliases and drop the module
                    field for builtins and name-only imports.  If the API is not
                    available in the current context, return an API triple of None.

                    :param api: API triple (language, module, function)
                    :param imports: list of available imports at the current point
                    """
                    language, module, function = api

                    if module == "builtins":
                        # For builtins, drop the module as no module is imported.
                        return (language, None, function)
                    else:
                        for imprt in imports:
                            if len(imprt) < 3:
                                import_module = imprt[0]
                                import_alias = imprt[1] if len(imprt) == 2 else None

                                match = search(
                                    f"^{import_module}(\\.[A-Za-z0-9._]+)?",
                                    module,
                                )
                                if match:
                                    # We imported the given module.  If an import
                                    # alias exists, use it in the return value.
                                    trailer = match.group(1) or ""
                                    if import_alias:
                                        module = import_alias + trailer
                                    else:
                                        module = import_module + trailer
                                    return (language, module, function)
                            else:
                                import_module, _, import_function = imprt
                                # For name-only imports, drop the module as no module
                                # is imported.
                                if (
                                    module == import_module
                                    and function == import_function
                                ):
                                    return (language, None, function)

                    return (None, None, None)

                def contextualize_api_usage(
                    tree: SimplifiedAST,
                    example_api: API_Type,
                    contextualized_api: API_Type,
                ) -> SimplifiedAST:
                    """
                    Return a new tree where API usages of the fully qualified,
                    unaliased @example_api are replaced with @contextualized_api
                    in @tree.  This allows us to contextualize a tree based on the
                    import names and aliases available at a given point in a program.

                    :param tree: simplified AST tree
                    :param example_api: API triple (language, module, function)
                    :param contextualized_api: API triple (language, module, function)
                    """

                    def api_to_tree(api: API_Type) -> SimplifiedAST:
                        """
                        Convert the given @api triple to a simplified AST
                        in the form it would be expected to appear in a
                        function call.

                        :param api: API triple (language, module, function)
                        """

                        def leaf(token: str) -> SimplifiedAST:
                            """
                            Create a simplified AST leaf from the given @token.

                            :param token: text for the AST leaf
                            """
                            data = {"token": token, "leading": "", "trailing": ""}
                            return SimplifiedAST(data=data, marked=True)

                        def dotted_name(token: str) -> SimplifiedAST:
                            """
                            Create a dotted name simplified AST from the given
                            token in the form ".@token".

                            :param token: text for the dotted name
                            """
                            children = []
                            children.append(leaf("."))
                            children.append(leaf(token))
                            return SimplifiedAST(
                                data=".#",
                                marked=True,
                                children=children,
                            )

                        language, module, function = api
                        if language == "python" and module:
                            # We are calling the function with the module name
                            # as part of the AST.  Create a simplified AST
                            # concatenating the module and function.
                            parts = module.split(".") + [function]

                            data = "#" * len(parts)
                            children = []
                            children.append(leaf(parts[0]))
                            for part in parts[1:]:
                                children.append(dotted_name(part))
                            return SimplifiedAST(
                                data=data,
                                marked=True,
                                children=children,
                            )
                        else:
                            # We are just calling the function directly,
                            # create a leaf node for the function.
                            return leaf(function)

                    def matches(a: SimplifiedAST, b: SimplifiedAST) -> bool:
                        """
                        Return TRUE if the tokens in @a match the tokens in @b.

                        :param a: simplified AST tree
                        :param b: simplified AST tree
                        """
                        match = a.token_or_label() == b.token_or_label()
                        if match:
                            match = len(a.children) == len(b.children)
                        if match:
                            for i in range(len(a.children)):
                                match = matches(a.children[i], b.children[i])
                                if not match:
                                    break
                        return match

                    def is_api_usage(tree: SimplifiedAST, api: SimplifiedAST) -> bool:
                        """
                        Return TRUE if @tree represents a function call to the
                        given @api

                        :param tree: simplified AST tree
                        :param api: simplified AST tree representing an API as it
                                    would appear in a function call
                        """
                        if api.children:
                            match = len(tree.children) - 1 == len(api.children)
                            if match:
                                for i in range(len(tree.children) - 1):
                                    match = matches(tree.children[i], api.children[i])
                                    if not match:
                                        break
                        else:
                            # The API is a simple identifier; check if the first node
                            # in the tree's children matches.
                            match = len(tree.children) == 2
                            if match:
                                match = matches(tree.children[0], api)

                        return match

                    def copy_whitespace(
                        a: SimplifiedAST,
                        b: SimplifiedAST,
                    ) -> SimplifiedAST:
                        """
                        Return a new tree with the leading/trailing whitespace copied
                        from @b into @a.

                        :param a: simplified AST tree
                        :param b: simplified AST tree
                        """
                        if isinstance(b.data, dict):
                            leading_ws = b.data["leading"]
                            trailing_ws = b.data["trailing"]
                        else:
                            leading_ws = ""
                            trailing_ws = ""

                        data = a.data
                        data["leading"] = leading_ws
                        data["trailing"] = trailing_ws
                        return SimplifiedAST(
                            data=data,
                            path=a.path,
                            parent=a.parent,
                            children=a.children,
                            marked=a.marked,
                        )

                    def replace_api_usage(
                        tree: SimplifiedAST,
                        old_api: SimplifiedAST,
                        new_api: SimplifiedAST,
                    ) -> SimplifiedAST:
                        """
                        Return a new tree where API usage subtrees of @old_api are
                        replaced with @new_api in @tree.

                        :param tree: simplified AST tree
                        :param old_api: simplified AST tree representing an API as it
                                        would appear in a function call
                        :param new_api: simplified AST tree representing an API as it
                                        would appear in a function call
                        """
                        if is_api_usage(tree, old_api):
                            # We found a usage of @old_api.  Replace tree with a
                            # new simplified AST tree with @old_api substituted
                            # for @new_api.
                            children = []
                            oc = tree.children[0]

                            if not new_api.children:
                                children.append(copy_whitespace(new_api, oc))
                            else:
                                nc = copy_whitespace(new_api.children[0], oc)
                                children.append(nc)
                                children.extend(new_api.children[1:])
                            children.append(tree.children[-1])
                            data = "#" * len(children)

                            return SimplifiedAST(
                                data=data,
                                path=tree.path,
                                marked=tree.marked,
                                children=children,
                            )
                        elif tree.children:
                            # Recursive case, replace API usage in subtrees.
                            return SimplifiedAST(
                                data=tree.data,
                                path=tree.path,
                                marked=tree.marked,
                                children=[
                                    replace_api_usage(c, old_api, new_api)
                                    for c in tree.children
                                ],
                            )
                        else:
                            # Leaf node, recursive stop.
                            return tree

                    return replace_api_usage(
                        tree,
                        api_to_tree(example_api),
                        api_to_tree(contextualized_api),
                    )

                # Find the example API usages in the database which (1) match
                # the identifier under the cursor and (2) are in scope (either
                # imported or builtin).
                matches = {}
                imports = name.imports(root)
                name_language, name_module, name_function = build_name_api(root, name)

                for example_api, examples in database.items():
                    contextualized_api = contextualize_api(example_api, imports)
                    ctx_language, ctx_module, ctx_function = contextualized_api

                    found = False
                    if ctx_language == name_language:
                        if ctx_module:
                            if (
                                (
                                    ctx_module == name_module
                                    and ctx_function
                                    and ctx_function.startswith(name_function)
                                )
                                or (
                                    name_module
                                    and not name_function
                                    and ctx_module.startswith(name_module)
                                )
                                or (
                                    not name_module
                                    and name_function
                                    and ctx_module.startswith(name_function)
                                )
                            ):
                                found = True
                        elif (
                            not name_module
                            and name_function
                            and ctx_function
                            and ctx_function.startswith(name_function)
                        ):
                            found = True

                    if found:
                        examples = [
                            contextualize_api_usage(
                                example, example_api, contextualized_api
                            )
                            for example in examples
                        ]
                        matches[contextualized_api] = examples

                return matches

            def completion_items(
                examples: Dict[API_Type, List[SimplifiedAST]]
            ) -> List[CompletionItem]:
                """
                Build a list of LSP completion items for the given dictionary
                of example API usage snippets.

                :param examples: dictionary mapping a given API to example usages
                """
                items = []

                # Sort the examples by the length of the API name (shortest first).
                examples = list(examples.items())
                examples = sorted(examples, key=lambda t: len(str(t[0])))

                # Once sorted, build the completion items to return.
                for api, trees in examples:
                    language, module, function = api
                    for i, tree in enumerate(trees):
                        if module is not None:
                            label = f"{module}.{function} ({chr(ord('A') + i)})"
                            filter_text = f"{module}.{function}"
                        else:
                            label = f"{function} ({chr(ord('A') + i)})"
                            filter_text = f"{function}"

                        item = CompletionItem(
                            label=label,
                            insert_text=tree.snippet(),
                            filter_text=filter_text,
                            kind=CompletionItemKind.Snippet,
                            insert_text_format=InsertTextFormat.Snippet,
                            documentation=MarkupContent(
                                kind=MarkupKind.Markdown,
                                value=code_block(tree.text(), language),
                            ),
                        )
                        items.append(item)

                return items

            # Parse the completion parameters for the information required
            # to respond to this completion request.
            workspace = server.workspace
            document = workspace.get_document(params.text_document.uri)
            filepath = Path(sub("file://", "", params.text_document.uri))
            position = (params.position.line + 1, params.position.character)
            language = guess_language(filepath)
            trigger = (
                params.context.trigger_kind
                if params.context
                else CompletionTriggerKind.Invoked
            )
            items = []

            # When the completion is explicitly invoked and the server
            # support snippets, find example API usages consistent with
            # the name under the cursor and the imports in scope.
            # Return these example snippets as completion items.
            if (
                trigger == CompletionTriggerKind.Invoked
                and language in SUPPORTED_LANGUAGES
                and has_snippet_support(server)
            ):
                # Parse the given file and find the name under the cursor.
                script = AST.from_string(
                    document.source,
                    ASTLanguage[language.capitalize()],
                )
                name = name_at_point(script, *position)

                # If the name does not appear in a context we wish
                # to skip, get example API usages consistent with the
                # name and return them as completion items.
                if not skip(script, name):
                    examples = examples(script, name, database)
                    items = completion_items(examples)

            return CompletionList(is_incomplete=False, items=items)

        if args.stdio:
            server.start_io()
        else:
            server.start_tcp("0.0.0.0", args.port)


if __name__ == "__main__":
    main()
