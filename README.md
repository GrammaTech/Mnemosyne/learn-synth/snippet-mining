# Description

Collection of techniques to present example code snippets to developers.

# Requirements

The snippet-mining repository requires python 3.6 or higher.
You will also need git-lfs which may be installed using the
instructions at https://git-lfs.github.com/.

# Installation

## Developer Install

If you are working on the development of this repository or
are a client looking for more frequent updates, you may wish
to install the package using the instructions given below.

The dependencies for this repository may be installed by executing
`pip3 install -r requirements.txt`.  If you are contributing you
will want to install the pre-commit hooks found in the
[pre-commit config](./pre-commit-config.yaml) by executing
`pre-commit install`.

After installing dependencies, you must place the repository's
src/ directory on your $PYTHONPATH.  From the base of this
repository, this may be accomplished with the following command:

```
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

## Client Install

If you are using the repository solely as a client, you may
create a python wheel file and install it by executing the
following sequence of commands:

```
python3 setup.py bdist_wheel --universal
pip3 install dist/*
```

Once installed, a `snippet-mining-server` command will be added to your
$PATH which may be invoked directly using the same interface and options
described in the snippet server section below.

# Command Line Interface

## Snippet Server

For developers, the `server.py` script in `src/snippet_mining/cmd` defines
a command line interface for this tool.  Help is available by running
`server.py --help`.

The script operates in two modes - LSP server mode (the default) and REST
mode (when `--rest` is passed).  In LSP mode, the server takes LSP completion
requests and returns potential example API usage snippets for the function
under the cursor if any applicable snippets are found.

In REST mode, the server takes POST requests with a JSON body like the following:
`{"language": "<language>", "module", "<module>", "function": "<function>"}`.
A full schema may be found in `examples-input.json`.  The REST API has two
resources; `database-examples` and `exempla-gratis-examples`.  The former
resource returns raw, unprocessed example API usages from a corpus of programs
whereas the later returns example API usages computed using the algorithm
described in "Exempla Gratis (E.G.): Code Examples for Free".
An example invocation of the REST API is given below:

```
curl -X POST \
        http://localhost:3039/exempla-gratis-examples \
        -H 'Content-Type: application/json' \
        -d '{"language": "python", "module": "json", "function": "dump"}'
```

The returned JSON has the form `{"result": List[Examples]}`.

Beyond this, the client may specify the port on which requests are
listened for by specifying the --port parameter; alternatively, the
client may pass --stdio to listen for requests on stdio in LSP
mode.
